FROM python:3.6.1
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update \
    && apt-get install -y postgresql-client-common postgresql-common postgresql-9.4 \
    && apt-get clean \
    && pip3 install psycopg2

ADD pg_hba.conf /etc/postgresql/9.4/main/