Build insturctions
------------------
```
docker build -t daniilr/python-postgres:latest .
docker push daniilr/python-postgres
```

Example Bitbucket Pipeline configuration
------------------
```
image: daniilr/python-postgres

pipelines:
  default:
    - step:
        script:
          - /etc/init.d/postgresql start
          - export DJANGO_SETTINGS_MODULE=project.settings
          - psql -U postgres -c "CREATE DATABASE testing_bd"
          - pip install -r requirements.txt
          - coverage run -m py.test
          - coverage report
```